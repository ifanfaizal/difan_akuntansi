﻿using GemBox.Spreadsheet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Program_Akuntansi
{
    public partial class MainForm : Form
    {
        private Welcome _welcome;

        public MainForm()
        {
            SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");
            _welcome = new Welcome();

            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            _welcome.Show();
        }
    }
}
