﻿namespace Program_Akuntansi
{
    partial class CreateCompanyAssistant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateCompanyAssistant));
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.menuLabel1 = new System.Windows.Forms.Label();
            this.menuLabel2 = new System.Windows.Forms.Label();
            this.menuLabel3 = new System.Windows.Forms.Label();
            this.menuLabel4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.tbNamaPerusahaan = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cbPeriode = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.cbBulanAwal = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.cbBulanAkhir = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tbTahunPembukuan = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.panelSummary = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelLokasiPenyimpanan = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panelSummary.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(567, 350);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 36);
            this.button1.TabIndex = 0;
            this.button1.Text = "LANJUT >";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(471, 350);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(90, 36);
            this.button2.TabIndex = 1;
            this.button2.Text = "< KEMBALI";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(375, 350);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(90, 36);
            this.button3.TabIndex = 2;
            this.button3.Text = "BATAL";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // menuLabel1
            // 
            this.menuLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuLabel1.Location = new System.Drawing.Point(86, 23);
            this.menuLabel1.Name = "menuLabel1";
            this.menuLabel1.Size = new System.Drawing.Size(95, 18);
            this.menuLabel1.TabIndex = 4;
            this.menuLabel1.Text = "Pengenalan";
            this.menuLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // menuLabel2
            // 
            this.menuLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuLabel2.Location = new System.Drawing.Point(9, 57);
            this.menuLabel2.Name = "menuLabel2";
            this.menuLabel2.Size = new System.Drawing.Size(172, 18);
            this.menuLabel2.TabIndex = 5;
            this.menuLabel2.Text = "Informasi Perusahaan";
            this.menuLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // menuLabel3
            // 
            this.menuLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuLabel3.Location = new System.Drawing.Point(-50, 92);
            this.menuLabel3.Name = "menuLabel3";
            this.menuLabel3.Size = new System.Drawing.Size(231, 18);
            this.menuLabel3.TabIndex = 6;
            this.menuLabel3.Text = "Informasi Akuntansi";
            this.menuLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // menuLabel4
            // 
            this.menuLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuLabel4.Location = new System.Drawing.Point(0, 127);
            this.menuLabel4.Name = "menuLabel4";
            this.menuLabel4.Size = new System.Drawing.Size(181, 18);
            this.menuLabel4.TabIndex = 7;
            this.menuLabel4.Text = "Berkas Perusahaan";
            this.menuLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Location = new System.Drawing.Point(187, 23);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(470, 321);
            this.panel1.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(-1, 52);
            this.label6.Name = "label6";
            this.label6.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label6.Size = new System.Drawing.Size(470, 91);
            this.label6.TabIndex = 1;
            this.label6.Text = resources.GetString("label6.Text");
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(-1, 0);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label5.Size = new System.Drawing.Size(470, 52);
            this.label5.TabIndex = 0;
            this.label5.Text = "Jendela ini digunakan untuk membantu Anda dalam membuat data perusahaan baru.";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.textBox4);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.textBox3);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.textBox2);
            this.panel2.Controls.Add(this.tbNamaPerusahaan);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Location = new System.Drawing.Point(187, 23);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(470, 321);
            this.panel2.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(123, 249);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 19);
            this.label12.TabIndex = 9;
            this.label12.Text = "* Wajib Diisi";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(123, 223);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(263, 20);
            this.textBox4.TabIndex = 8;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(-5, 223);
            this.label11.Name = "label11";
            this.label11.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label11.Size = new System.Drawing.Size(122, 19);
            this.label11.TabIndex = 7;
            this.label11.Text = "Alamat Email";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(123, 187);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(263, 20);
            this.textBox3.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(-5, 187);
            this.label10.Name = "label10";
            this.label10.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label10.Size = new System.Drawing.Size(122, 19);
            this.label10.TabIndex = 5;
            this.label10.Text = "Nomor Telepon";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(124, 83);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(263, 88);
            this.textBox2.TabIndex = 4;
            // 
            // tbNamaPerusahaan
            // 
            this.tbNamaPerusahaan.Location = new System.Drawing.Point(124, 47);
            this.tbNamaPerusahaan.Name = "tbNamaPerusahaan";
            this.tbNamaPerusahaan.Size = new System.Drawing.Size(263, 20);
            this.tbNamaPerusahaan.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(-4, 83);
            this.label9.Name = "label9";
            this.label9.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label9.Size = new System.Drawing.Size(122, 19);
            this.label9.TabIndex = 2;
            this.label9.Text = "Alamat";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(-4, 47);
            this.label7.Name = "label7";
            this.label7.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label7.Size = new System.Drawing.Size(122, 19);
            this.label7.TabIndex = 1;
            this.label7.Text = "Nama Perusahaan*";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(-1, 13);
            this.label8.Name = "label8";
            this.label8.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label8.Size = new System.Drawing.Size(470, 26);
            this.label8.TabIndex = 0;
            this.label8.Text = "Masukkan informasi mengenai perusahaan Anda";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.cbPeriode);
            this.panel3.Controls.Add(this.label19);
            this.panel3.Controls.Add(this.cbBulanAwal);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.cbBulanAkhir);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.tbTahunPembukuan);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Location = new System.Drawing.Point(187, 23);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(470, 321);
            this.panel3.TabIndex = 10;
            // 
            // cbPeriode
            // 
            this.cbPeriode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPeriode.FormattingEnabled = true;
            this.cbPeriode.Items.AddRange(new object[] {
            "12",
            "13"});
            this.cbPeriode.Location = new System.Drawing.Point(123, 223);
            this.cbPeriode.Name = "cbPeriode";
            this.cbPeriode.Size = new System.Drawing.Size(46, 21);
            this.cbPeriode.TabIndex = 14;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label19.Location = new System.Drawing.Point(-35, 223);
            this.label19.Name = "label19";
            this.label19.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label19.Size = new System.Drawing.Size(192, 19);
            this.label19.TabIndex = 13;
            this.label19.Text = "Periode pembukuan";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbBulanAwal
            // 
            this.cbBulanAwal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbBulanAwal.FormattingEnabled = true;
            this.cbBulanAwal.Items.AddRange(new object[] {
            "Januari",
            "Februari",
            "Maret",
            "April",
            "Mei",
            "Juni",
            "Juli",
            "Agustus",
            "September",
            "Oktober",
            "November",
            "Desember"});
            this.cbBulanAwal.Location = new System.Drawing.Point(193, 176);
            this.cbBulanAwal.Name = "cbBulanAwal";
            this.cbBulanAwal.Size = new System.Drawing.Size(76, 21);
            this.cbBulanAwal.TabIndex = 12;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label16.Location = new System.Drawing.Point(-12, 175);
            this.label16.Name = "label16";
            this.label16.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label16.Size = new System.Drawing.Size(192, 19);
            this.label16.TabIndex = 11;
            this.label16.Text = "Bulan awal tahun pembukuan";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label17.Location = new System.Drawing.Point(-15, 149);
            this.label17.Name = "label17";
            this.label17.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label17.Size = new System.Drawing.Size(223, 19);
            this.label17.TabIndex = 10;
            this.label17.Text = "Bulan apa dimulainya pembukuan?";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbBulanAkhir
            // 
            this.cbBulanAkhir.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbBulanAkhir.FormattingEnabled = true;
            this.cbBulanAkhir.Items.AddRange(new object[] {
            "Januari",
            "Februari",
            "Maret",
            "April",
            "Mei",
            "Juni",
            "Juli",
            "Agustus",
            "September",
            "Oktober",
            "November",
            "Desember"});
            this.cbBulanAkhir.Location = new System.Drawing.Point(193, 110);
            this.cbBulanAkhir.Name = "cbBulanAkhir";
            this.cbBulanAkhir.Size = new System.Drawing.Size(76, 21);
            this.cbBulanAkhir.TabIndex = 9;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label15.Location = new System.Drawing.Point(-5, 110);
            this.label15.Name = "label15";
            this.label15.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label15.Size = new System.Drawing.Size(192, 19);
            this.label15.TabIndex = 8;
            this.label15.Text = "Bulan terakhir tahun pembukuan";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label14.Location = new System.Drawing.Point(-1, 83);
            this.label14.Name = "label14";
            this.label14.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label14.Size = new System.Drawing.Size(223, 19);
            this.label14.TabIndex = 6;
            this.label14.Text = "Bulan apa tahun finansial Anda berakhir?";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label13.Location = new System.Drawing.Point(-1, 42);
            this.label13.Name = "label13";
            this.label13.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label13.Size = new System.Drawing.Size(117, 19);
            this.label13.TabIndex = 5;
            this.label13.Text = "Tahun pembukuan";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbTahunPembukuan
            // 
            this.tbTahunPembukuan.Location = new System.Drawing.Point(124, 42);
            this.tbTahunPembukuan.Name = "tbTahunPembukuan";
            this.tbTahunPembukuan.Size = new System.Drawing.Size(45, 20);
            this.tbTahunPembukuan.TabIndex = 4;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(-1, 13);
            this.label18.Name = "label18";
            this.label18.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label18.Size = new System.Drawing.Size(470, 26);
            this.label18.TabIndex = 0;
            this.label18.Text = "Beritahu kami tentang tahun akuntansi Anda";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelSummary
            // 
            this.panelSummary.BackColor = System.Drawing.Color.White;
            this.panelSummary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelSummary.Controls.Add(this.label25);
            this.panelSummary.Controls.Add(this.label23);
            this.panelSummary.Controls.Add(this.label22);
            this.panelSummary.Controls.Add(this.label21);
            this.panelSummary.Controls.Add(this.label20);
            this.panelSummary.Controls.Add(this.label24);
            this.panelSummary.Controls.Add(this.label26);
            this.panelSummary.Location = new System.Drawing.Point(187, 23);
            this.panelSummary.Name = "panelSummary";
            this.panelSummary.Size = new System.Drawing.Size(470, 321);
            this.panelSummary.TabIndex = 11;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label25.Location = new System.Drawing.Point(-1, 246);
            this.label25.Name = "label25";
            this.label25.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label25.Size = new System.Drawing.Size(470, 27);
            this.label25.TabIndex = 11;
            this.label25.Text = "Jika informasi sudah benar, tekan tombol Lanjut.";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label23.Location = new System.Drawing.Point(-1, 223);
            this.label23.Name = "label23";
            this.label23.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label23.Size = new System.Drawing.Size(470, 27);
            this.label23.TabIndex = 10;
            this.label23.Text = "Jika terdapat kekeliruan dalam informasi di atas, silakan tekan tombol Kembali.";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label22.Location = new System.Drawing.Point(-1, 157);
            this.label22.Name = "label22";
            this.label22.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label22.Size = new System.Drawing.Size(470, 27);
            this.label22.TabIndex = 9;
            this.label22.Text = "Anda tidak dapat melakukan transaksi apapun sebelum tanggal tersebut.";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label21.Location = new System.Drawing.Point(-1, 133);
            this.label21.Name = "label21";
            this.label21.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label21.Size = new System.Drawing.Size(470, 27);
            this.label21.TabIndex = 8;
            this.label21.Text = "Bulan konversi pembukuan Anda adalah _.";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label20.Location = new System.Drawing.Point(-1, 69);
            this.label20.Name = "label20";
            this.label20.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label20.Size = new System.Drawing.Size(470, 27);
            this.label20.TabIndex = 7;
            this.label20.Text = "Anda telah memilih periode akuntansi _ per tahun pembukuan.";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label24.Location = new System.Drawing.Point(-1, 47);
            this.label24.Name = "label24";
            this.label24.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label24.Size = new System.Drawing.Size(470, 27);
            this.label24.TabIndex = 6;
            this.label24.Text = "Tahun pembukuan Anda adalah dari ";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(-1, 13);
            this.label26.Name = "label26";
            this.label26.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label26.Size = new System.Drawing.Size(470, 26);
            this.label26.TabIndex = 0;
            this.label26.Text = "Tolong konfirmasi tahun pembukuan Anda";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.button4);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.labelLokasiPenyimpanan);
            this.panel4.Controls.Add(this.label28);
            this.panel4.Controls.Add(this.label29);
            this.panel4.Location = new System.Drawing.Point(187, 23);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(470, 321);
            this.panel4.TabIndex = 12;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(6, 134);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 12;
            this.button4.Text = "UBAH";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label1.Location = new System.Drawing.Point(-1, 167);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label1.Size = new System.Drawing.Size(470, 27);
            this.label1.TabIndex = 11;
            this.label1.Text = "Jika informasi sudah benar, tekan tombol Lanjut.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label3.Location = new System.Drawing.Point(80, 132);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label3.Size = new System.Drawing.Size(374, 27);
            this.label3.TabIndex = 9;
            this.label3.Text = "Tekan tombol di samping apabila ingin merubah lokasi penyimpanan";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelLokasiPenyimpanan
            // 
            this.labelLokasiPenyimpanan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLokasiPenyimpanan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelLokasiPenyimpanan.Location = new System.Drawing.Point(3, 96);
            this.labelLokasiPenyimpanan.Name = "labelLokasiPenyimpanan";
            this.labelLokasiPenyimpanan.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.labelLokasiPenyimpanan.Size = new System.Drawing.Size(470, 27);
            this.labelLokasiPenyimpanan.TabIndex = 8;
            this.labelLokasiPenyimpanan.Text = "$LOKASI";
            this.labelLokasiPenyimpanan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label28.Location = new System.Drawing.Point(-1, 47);
            this.label28.Name = "label28";
            this.label28.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label28.Size = new System.Drawing.Size(470, 39);
            this.label28.TabIndex = 6;
            this.label28.Text = "Nama berkas yang akan disimpan sesuai dengan nama perusahaan Anda. Lokasi penyimp" +
    "anan ada di dalam lokasi program Difan.";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(-1, 13);
            this.label29.Name = "label29";
            this.label29.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label29.Size = new System.Drawing.Size(470, 26);
            this.label29.TabIndex = 0;
            this.label29.Text = "Buat berkas perusahaan Anda";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(567, 350);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(90, 36);
            this.button5.TabIndex = 13;
            this.button5.Text = "SELESAI";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // CreateCompanyAssistant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(669, 398);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panelSummary);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuLabel4);
            this.Controls.Add(this.menuLabel3);
            this.Controls.Add(this.menuLabel2);
            this.Controls.Add(this.menuLabel1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "CreateCompanyAssistant";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Buat Perusahaan Baru";
            this.Load += new System.EventHandler(this.Create_Company_Assistant_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panelSummary.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label menuLabel1;
        private System.Windows.Forms.Label menuLabel2;
        private System.Windows.Forms.Label menuLabel3;
        private System.Windows.Forms.Label menuLabel4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox tbNamaPerusahaan;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cbBulanAkhir;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tbTahunPembukuan;
        private System.Windows.Forms.ComboBox cbPeriode;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox cbBulanAwal;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panelSummary;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelLokasiPenyimpanan;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button button5;
    }
}