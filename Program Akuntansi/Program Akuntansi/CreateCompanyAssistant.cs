﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Program_Akuntansi
{
    public partial class CreateCompanyAssistant : Form
    {
        private int _currentIndex = 0;
        private bool _showSummaryAccInformation = false;

        public CreateCompanyAssistant()
        {
            InitializeComponent();
        }

        private void Create_Company_Assistant_Load(object sender, EventArgs e)
        {
            foreach (Control x in this.Controls)
            {
                if (x is Label)
                {
                    if (((Label)x).Name == "menuLabel1")
                        ((Label)x).Font = new Font("Microsoft Sans Serif", 11, FontStyle.Bold);
                    else
                        ((Label)x).Font = new Font("Microsoft Sans Serif", 11, FontStyle.Regular);
                }
                else if (x is Panel)
                {
                    if (((Panel)x).Name == "panel1")
                        ((Panel)x).Visible = true;
                    else
                        ((Panel)x).Visible = false;
                }
            }

            // Tombol lanjut
            button2.Enabled = false;

            button5.Visible = false;

            // Tahun pembukuan
            tbTahunPembukuan.Text = "2019";

            // Bulan terakhir pembukuan
            cbBulanAkhir.SelectedIndex = 0;

            // Bulan awal pembukuan
            cbBulanAwal.SelectedIndex = 0;

            // Periode pembukuan
            cbPeriode.SelectedIndex = 0;
        }

        // Transisi tampilan asisten
        private void changePage()
        {
            foreach (Control x in this.Controls)
            {
                if (x is Label)
                {
                    if (((Label)x).Name == $"menuLabel{_currentIndex + 1}")
                        ((Label)x).Font = new Font("Microsoft Sans Serif", 11, FontStyle.Bold);
                    else
                        ((Label)x).Font = new Font("Microsoft Sans Serif", 11, FontStyle.Regular);
                }
                else if (x is Panel)
                {
                    if (((Panel)x).Name == $"panel{_currentIndex + 1}")
                        ((Panel)x).Visible = true;
                    else
                        ((Panel)x).Visible = false;
                }
            }

            labelLokasiPenyimpanan.Text = Application.StartupPath + $"\\files\\{tbNamaPerusahaan.Text}.json";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Kalau sedang di menu Informasi Akuntansi (_currentIndex == 2), kita perlu menampilkan summary ketika user pilih lanjut
            if (_currentIndex == 2 && !_showSummaryAccInformation)
            {
                label20.Text = label20.Text.Replace("_", cbPeriode.SelectedItem.ToString());

                var conversionMonth = new DateTime(Convert.ToInt32(tbTahunPembukuan.Text), cbBulanAwal.SelectedIndex + 1, 1);
                label21.Text = label21.Text.Replace("_", conversionMonth.ToString("d MMMM yyyy"));

                var lastMonth = cbBulanAkhir.SelectedIndex + 1 == 13 ? 0 : cbBulanAkhir.SelectedIndex + 1; // Kalo 13 berarti desember, harus ambil index ke 0 (januari)
                var lastMonthAccounting = new DateTime(Convert.ToInt32(tbTahunPembukuan.Text), lastMonth, 1);
                var lastDayOfLastMonthAccounting = lastMonthAccounting.AddMonths(1).AddDays(-1);

                var firstMonthAccounting = lastMonthAccounting.AddMonths(-11);
                var firstDayOfFirstMonthAccounting = new DateTime(firstMonthAccounting.Year, firstMonthAccounting.Month, 1);

                label24.Text = $"Tahun pembukuan Anda adalah dari {firstDayOfFirstMonthAccounting.ToString("d MMMM yyyy")} " +
                    $"sampai {lastDayOfLastMonthAccounting.ToString("d MMMM yyyy")}.";

                panelSummary.Visible = true;
                _showSummaryAccInformation = true;
            }
            else
            {
                _currentIndex++;
                changePage();

                if (_currentIndex > 0)
                    button2.Enabled = true;

                if (_currentIndex > 2)
                {
                    button1.Visible = false;
                    button5.Visible = true;
                }

                panelSummary.Visible = false;
                _showSummaryAccInformation = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (_currentIndex == 2 && _showSummaryAccInformation)
            {
                // Kalau sedang di menu Informasi Akuntansi, tapi berada pada halaman summary acc
                panelSummary.Visible = false;
                _showSummaryAccInformation = false;
            }
            else if (_currentIndex == 3 && !_showSummaryAccInformation)
            {
                // Kalau sedang di menu Daftar Akun, kita perlu menampilkan summary ketika user pilih kembali
                panelSummary.Visible = true;
                _showSummaryAccInformation = true;
                panel4.Visible = false;
            }
            else
            {
                _currentIndex--;
                changePage();

                if (_currentIndex == 0)
                    button2.Enabled = false;

                if (_currentIndex <= 2)
                {
                    button1.Visible = true;
                    button5.Visible = false;
                }

                panelSummary.Visible = false;
                _showSummaryAccInformation = false;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    labelLokasiPenyimpanan.Text = fbd.SelectedPath;
                }
            }
        }
    }
}
