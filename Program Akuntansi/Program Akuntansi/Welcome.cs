﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Program_Akuntansi
{
    public partial class Welcome : Form
    {
        private CreateCompanyAssistant _ccaWindow;

        public Welcome()
        {
            _ccaWindow = new CreateCompanyAssistant();

            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.SendToBack();
            _ccaWindow.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
